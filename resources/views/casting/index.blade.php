@extends('layout.master')
@section('content')
    
    <table id="example1" class="table table table-bordered table-striped">
        <thead>
            <tr>
                <td style="text-align: center; font-weight:bold">No</td>
                <td style="text-align: center; font-weight:bold">Nama</td>
                <td style="text-align: center; font-weight:bold">Umur</td>
                <td style="text-align: center; font-weight:bold">Bio</td>
            </tr>
        </thead>
        <tbody>
            @forelse ($movie as $key => $value)
            <tr>
                <td>{{ $key+1 }}</td>
                <td>{{$value->nama }}</td>
                <td>{{$value->umur }}</td>
                <td>{{$value->bio }}</td>
            </tr>
            @empty
            <tr>
                <td colspan="4" align="center">No Data </td>
            </tr>
            @endforelse
        </tbody>
    </table>
@endsection
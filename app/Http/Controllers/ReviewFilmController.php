<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class ReviewFilmController extends Controller
{
    public function casting()
    {
        $movie = DB::table('cast')->get();
        return view('casting.index', compact('movie'), [
            'title' => 'Film REVIEW',
            'name' => 'FIlm Review'
        ]);
    }

    public function genre()
    {
        $movie = DB::table('genre')->get();
        return view('genre.index', compact('movie'), [
            'title' => 'Film REVIEW',
            'name' => 'FIlm Review'
        ]);
    }

    public function film()
    {
        $movie = DB::table('film')->get();
        return view('film.index', compact('movie'), [
            'title' => 'Film REVIEW',
            'name' => 'FIlm Review'
        ]);
    }
}
